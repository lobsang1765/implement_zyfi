
import { http, createConfig } from 'wagmi'
import { zkSync, sepolia } from 'wagmi/chains'

export const config = createConfig({
  chains: [zkSync, sepolia],
  transports: {
    [zkSync.id]: http(),
    [sepolia.id]: http(),
  },
})