"use client"
import { useAccount , useConnect} from 'wagmi'
import { KreatorlandABI } from "@/utils/StorefrontABI";
import { Contract} from 'zksync-web3';
import { useWalletClient } from 'wagmi';
import { Web3Provider } from 'zksync-web3';
import { useCallback } from 'react';
import { useDisconnect } from 'wagmi'
export function walletClientToSigner(walletClient:any) {
 
  const { account, chain, transport } = walletClient;
  const network = {
    chainId: chain.id,
    name: chain.name,
    ensAddress: chain.contracts?.ensRegistry?.address,
  };
  const provider = new Web3Provider(transport, network);
  const signer = provider.getSigner(account.address);
  return signer;

}

function App() {
  const { address, isConnected } = useAccount();
  const MINT_AMT = 100000000000000;
  const PAYMASTER_ADDRESS = '0x4081e092F948Cffd946a75e1F556c13c372304bc';
  const ERC20_PAY_GAS_ADDRESS = "0x3355df6d4c9c3035724fd0e3914de96a5a83aaf4";
  const KND_STOREFRONT = "0x9161c622D6A8FA99555e82201987CC2574e4335D";
  const media_root = 'https://cloudflare-ipfs.com/ipfs/1/metadata.json';
  const RPC_URL = 'https://mainnet.era.zksync.io';
  const HOLDING_FEE = "0.0013";
  const IDENTIFICATION_CODE = 9002;
  const SEND_BALANCE = "0.001";
  const { data: walletClient } = useWalletClient();
  const {connectors, connect} = useConnect()
  const { disconnect } = useDisconnect()

  async function postTransactionData(payload:any) {
    try {
      const response = await fetch('https://api.zyfi.org/api/erc20_paymaster/v1', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload),
      });
  
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
  
      const data = await response.json();
      console.log(data); // Process the response data

      return data;
    } catch (error) {
      console.error('Error during the API call:', error);
    }
  }


  const onPressKreatorLand = useCallback(async () => {
    const signer = walletClientToSigner(walletClient);

    const contract = new Contract(KND_STOREFRONT, KreatorlandABI, signer);

    const populateContract = await contract.populateTransaction['mint'](
      media_root,
      1,
      MINT_AMT,
      {
        from: signer._address,
        value: MINT_AMT,
      }
    );
    // Define the payload
    const payload = {
      feeTokenAddress: ERC20_PAY_GAS_ADDRESS,
      // "gasLimit": "500000",
      "isTestnet": false,
      "checkNFT": false,
      txData: {
        from: "0x3111A4c99f918D7A8a61938c8E97c67C5a124277",
        to: "0xA2Aac7bC9725c36ad9B12D2407dF8de6B2B68359",
        value: "0",
        data: populateContract.data
      }
    }

    let apiResponse = await postTransactionData(payload)
    console.log("api response", apiResponse)

    await signer.sendTransaction(apiResponse.txData)
  }, [walletClient]);
  return (
    <div>
    {
      !isConnected ?  (
        connectors.map((connector)=> (
          <button key={connector.id} onClick={()=> connect({connector})}>{connector.name}</button>
        ))
      ):(
        <>
        <button onClick={()=> onPressKreatorLand()}>mint</button>
        <button onClick={() => disconnect()}>
      Disconnect
    </button>
        </>
      )
    }
    </div>
    
  )
  

}

export default App;