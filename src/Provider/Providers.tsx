"use client"
import { http, createConfig } from 'wagmi'
import { mainnet, sepolia, zkSync } from 'wagmi/chains'
import { WagmiProvider } from 'wagmi' ;
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import {  coinbaseWallet } from 'wagmi/connectors';
import { walletConnect } from 'wagmi/connectors'
const projectId = '68b63a5f47ce57460ca4665a0512d147'

  const config = createConfig({
    chains: [mainnet, zkSync],
    ssr: true, 
    connectors: [
      coinbaseWallet({
        appName:"kreatoland"
      }),
      walletConnect({
        projectId: "68b63a5f47ce57460ca4665a0512d147",
        showQrModal: true, 
      })
    ],
    transports: {
      [mainnet.id]: http(),
      [zkSync.id]: http(),
    },
  })
  
  const queryClient = new QueryClient() 


export const ProviderWagmi =({children}:any) =>  {
    console.log("mainnet", config)
  return (
    <WagmiProvider config={config}> 
    
        <QueryClientProvider client={queryClient}> 
            {children} 
        </QueryClientProvider> 

    </WagmiProvider>
  )
    
}